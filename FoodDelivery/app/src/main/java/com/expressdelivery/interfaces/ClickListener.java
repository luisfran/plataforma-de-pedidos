package com.expressdelivery.interfaces;

public interface ClickListener {
    void onClick(int position);
}