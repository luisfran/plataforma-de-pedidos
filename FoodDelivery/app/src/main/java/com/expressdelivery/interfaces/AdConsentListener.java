package com.expressdelivery.interfaces;

public interface AdConsentListener {
    void onConsentUpdate();
}
