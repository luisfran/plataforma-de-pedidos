package com.expressdelivery.interfaces;

import com.expressdelivery.items.ItemMenuCat;

import java.util.ArrayList;

public interface MenuCatListener {
    void onStart();
    void onEnd(String success, String verifyStatus, String message, ArrayList<ItemMenuCat> arrayListMenuCat);
}
