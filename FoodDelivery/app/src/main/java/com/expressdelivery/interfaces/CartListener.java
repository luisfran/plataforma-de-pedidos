package com.expressdelivery.interfaces;

import com.expressdelivery.items.ItemCart;

import java.util.ArrayList;

public interface CartListener {
    void onStart();
    void onEnd(String success, String verifyStatus, String message, ArrayList<ItemCart> arrayListMenu);
}