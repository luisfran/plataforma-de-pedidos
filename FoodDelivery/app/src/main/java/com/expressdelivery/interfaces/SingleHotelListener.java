package com.expressdelivery.interfaces;

import com.expressdelivery.items.ItemRestaurant;

public interface SingleHotelListener {
    void onStart();
    void onEnd(String success, String verifyStatus, String message, ItemRestaurant itemRestaurant);
}