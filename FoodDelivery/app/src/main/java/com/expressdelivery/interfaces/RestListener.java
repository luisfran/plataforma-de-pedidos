package com.expressdelivery.interfaces;

import com.expressdelivery.items.ItemRestaurant;

import java.util.ArrayList;

public interface RestListener {
    void onStart();
    void onEnd(String success, String verifyStatus, String message, ArrayList<ItemRestaurant> arrayListRestaurant);
}
