package com.expressdelivery.fooddelivery;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.expressdelivery.asyncTask.LoadAbout;
import com.expressdelivery.asyncTask.LoadLogin;
import com.expressdelivery.interfaces.AboutListener;
import com.expressdelivery.interfaces.LoginListener;
import com.expressdelivery.items.ItemUser;
import com.expressdelivery.utils.SharedPref;
import com.expressdelivery.utils.Constant;
import com.expressdelivery.utils.DBHelper;
import com.expressdelivery.utils.Methods;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    SharedPref sharedPref;
    Methods methods;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        hideStatusBar();

        sharedPref = new SharedPref(this);
        methods = new Methods(this);
        dbHelper = new DBHelper(this);

        if (sharedPref.getIsFirst()) {
            loadAboutData();
        } else {
            if (sharedPref.getIsFirst()) {
                openLoginActivity();
            } else {
                if (!sharedPref.getIsAutoLogin()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            openMainActivity();
                        }
                    }, 2000);
                } else {
                    if (methods.isNetworkAvailable()) {
                        loadLogin();
                    } else {
                        openMainActivity();
                    }
                }
            }
        }
    }

    private void loadLogin() {
        if (methods.isNetworkAvailable()) {
            LoadLogin loadLogin = new LoadLogin(new LoginListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onEnd(String success, String loginSuccess, String message, String user_id, String user_name) {

                    if (success.equals("1")) {
                        if (loginSuccess.equals("1")) {
                            Constant.itemUser = new ItemUser(user_id, user_name, sharedPref.getEmail(), sharedPref.getMobile(), sharedPref.getCity(), sharedPref.getAddress());

                            Constant.isLogged = true;
                            openMainActivity();
                        } else {
                            openMainActivity();
                        }
                    } else {
                        openMainActivity();
                    }
                }
            }, methods.getAPIRequest(Constant.METHOD_LOGIN, 0, "", "", "", "", "", "","", "","", sharedPref.getEmail(), sharedPref.getPassword(), "", "", "", "", "", "", null));
            loadLogin.execute();
        } else {
            methods.showToast(getString(R.string.error_net_not_conn));
        }
    }

    public void loadAboutData() {
        if (methods.isNetworkAvailable()) {
            LoadAbout loadAbout = new LoadAbout(SplashActivity.this, new AboutListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onEnd(String success, String verifyStatus, String message) {
                    if (success.equals("1")) {
                        if (!verifyStatus.equals("-1")) {
                            dbHelper.addtoAbout();
                            if (sharedPref.getIsFirst()) {
                                sharedPref.setIsFirst(false);
                                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                intent.putExtra("from","");
                                startActivity(intent);
                                finish();
                            } else {
                                openMainActivity();
                            }
                        } else {
                            errorDialog(getString(R.string.error_unauth_access), message);
                        }
                    } else {
                        errorDialog(getString(R.string.error_server), getString(R.string.error_server_conneting));
                    }
                }
            });
            loadAbout.execute();
        } else {
            errorDialog(getString(R.string.error_net_not_conn), getString(R.string.error_connect_net_tryagain));
        }
    }

    private void errorDialog(String title, String message) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashActivity.this, R.style.ThemeDialog);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);

        if (title.equals(getString(R.string.error_net_not_conn)) || title.equals(getString(R.string.error_server))) {
            alertDialog.setNegativeButton(getString(R.string.try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    loadAboutData();
                }
            });
        }

        alertDialog.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    private void openLoginActivity() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        intent.putExtra("from","");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void openMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    void hideStatusBar() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }
}