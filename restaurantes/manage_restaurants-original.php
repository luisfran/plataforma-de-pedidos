<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	 if(isset($_POST['cat_search']))
   {
    
      $keyword=addslashes(trim($_POST['search_value']));


      $sql="SELECT * FROM tbl_restaurants
          LEFT JOIN tbl_category ON tbl_category.`cid`= tbl_restaurants.`cat_id` 
          WHERE tbl_restaurants.`restaurant_name` LIKE '%$keyword%' ORDER BY tbl_restaurants.`restaurant_name` DESC"; 

      $result=mysqli_query($mysqli,$sql);
    
   }
   else
   {
      $tableName="tbl_restaurants";   
      $targetpage = "manage_restaurants.php"; 
      $limit = 10; 
      
      $query = "SELECT COUNT(*) as num FROM $tableName LEFT JOIN tbl_category ON tbl_category.cid= tbl_restaurants.cat_id ORDER BY tbl_restaurants.restaurant_name DESC";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];
      
      $stages = 1;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
        $start = ($page - 1) * $limit; 
      }else{
        $start = 0; 
      } 
      $quotes_qry="";
      if($_SESSION['type']==1){
        $quotes_qry="SELECT * FROM tbl_restaurants
        LEFT JOIN tbl_category ON tbl_category.cid= tbl_restaurants.cat_id 
        ORDER BY tbl_restaurants.id DESC LIMIT $start, $limit"; 
      } else {
        $quotes_qry="SELECT * FROM tbl_restaurants
        LEFT JOIN tbl_category ON tbl_category.cid= tbl_restaurants.cat_id
        WHERE id ='".$_SESSION['id_restaurant']."'"; 
      }
     
     $result=mysqli_query($mysqli,$quotes_qry);


   }
	 
?>
                
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Administrar Restaurantes
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">

              <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 

                <!--begin: Search Form -->

                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                      <div class="form-group m-form__group row align-items-center">
                        <form  method="post" action="" class="m-form">

                            <div class="col-md-12">
                              <div class="m-input-icon m-input-icon--left">
                                <?php if($_SESSION['type']==1){?>
                                  <div class="input-group">
                                    <input type="text" name="search_value" class="form-control form-control-warning" placeholder="Buscar por..." required autocomplete="off">
                                    <span class="input-group-btn">
                                      <button class="btn btn-brand" type="submit" name="cat_search">
                                        Go!
                                      </button>
                                    </span>
                                  </div>
                                <?php }?>
                              </div>
                            </div>
                        </form>    
                      </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <?php if($_SESSION['type']==1){?>
                        <a href="add_restaurant.php?add=yes" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                          <span>
                            <i class="la la-plus"></i>
                            <span>
                              Agregar Restaurante
                            </span>
                          </span>
                        </a>
                      <?php }?>
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                    <table class="table">
              <thead class="thead-default">
                <tr>                  
                   <th>Categoria</th>
                   <th>Nombre del Restaurante</th>
                   <th>Imagen del Restaurante</th>
                   <th>Featured</th>
                   <th>Status</th>
                   <th>Menu</th>
                  <th class="cat_action_list">Acciones</th>
                </tr>
              </thead>
              <tbody>
                <?php 
            $i=0;
            while($row=mysqli_fetch_array($result))
            {         
        ?>
                <tr scope="row">                 
                  <td>
                    <?php echo $row['category_name'];?>    
                  </td>
                  <td>
                    <a href="add_restaurant.php?restaurant_id=<?php echo $row['id'];?>" style="text-decoration: none;"><?php echo $row['restaurant_name'];?></a>
                  </td>
                  <td>
                    <img src="images/<?php echo $row['restaurant_image'];?>" width="150" height="100" />
                  </td>
                  <td>
                    <?php if($row['featured_restaurant']!="0"){?>
                      <?php if($_SESSION['type']==1){?>
                        <a class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$row['id']?>" data-action="deactive" data-column="featured_restaurant" title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;
                        font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Enable</span></span></a>
                      <?php } else {?>
                        <a  href="#"  title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;
                        font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Enable</span></span></a>
                      <?php }?>
                    <?php }else{?>
                      <?php if($_SESSION['type']==1){?>
                        <a class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$row['id']?>" data-action="active" data-column="featured_restaurant" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Inactivo </span></span></a>
                      <?php } else {?>
                        <a href="#" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Inactivo </span></span></a>
                      <?php }?>
                    <?php }?>
                  </td>

                  <td>
                  <?php if($row['status']!="0"){?>
                    <?php if($_SESSION['type']==1){?>
                      <a  class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$row['id']?>" data-action="deactive" data-column="status" title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Activo</span></span></a>
                    <?php } else {?>
                      <a  href="#"  title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Activo</span></span></a>
                    <?php }?>
                  <?php }else{?>
                    <?php if($_SESSION['type']==1){?>
                      <a href="" class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$row['id']?>" data-action="active" data-column="status" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Inactivo </span></span></a>
                    <?php } else {?>
                      <a  href="#" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Inactivo </span></span></a>
                    <?php }?>
                  <?php }?>
                  </td>

                  <td>
                    <a href="restaurant_view.php?restaurant_id=<?php echo $row['id'];?>" title="Ver Menu Restaurante"><span class="badge badge-success badge-icon"><i class="fa fa-eye" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Ver Menu</span></span></a>
                  </td>  
                  <td> 

                    <a href="add_restaurant.php?restaurant_id=<?php echo $row['id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar detalles">
                      <i class="la la-edit"></i>
                    </a>
                    <?php if($_SESSION['type']==1){?>
                      <a href="" data-id="<?php echo $row['id'];?>" class="btn_delete_a m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar">
                        <i class="la la-trash"></i>
                      </a>
                    <?php }?> 
                </tr>
                <?php
            
            $i++;
              }
        ?> 
              </tbody>
            </table>

            </div>
              <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php if(!isset($_POST["cat_search"])){ include("pagination.php");}?>
                  </nav>
                </div>
              </div>


                <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>  

<script type="text/javascript">

  $(".toggle_btn_a").on("click",function(e){
    e.preventDefault();
    
    var _for=$(this).data("action");
    var _id=$(this).data("id");
    var _column=$(this).data("column");
    var _table='tbl_restaurants';

    $.ajax({
      type:'post',
      url:'processData.php',
      dataType:'json',
      data:{id:_id,for_action:_for,column:_column,table:_table,'action':'toggle_status','tbl_id':'id'},
      success:function(res){
          console.log(res);
          if(res.status=='1'){
            location.reload();
          }
        }
    });

  });

  $(".btn_delete_a").click(function(e){
      e.preventDefault();

      var _ids = $(this).data("id");

      if(_ids!='')
      {
        if(confirm("Do you really want to delete this?")){
          $.ajax({
            type:'post',
            url:'processData.php',
            dataType:'json',
            data:{id:_ids,'action':'multi_delete','tbl_nm':'tbl_restaurants'},
            success:function(res){
                console.log(res);
                if(res.status=='1'){
                  location.reload();
                }
                else if(res.status=='-2'){
                  alert(res.message);
                }
              }
          });
        }
      }
  });

</script>     
