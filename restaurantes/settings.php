<?php include("includes/header.php");

require("includes/function.php");
require("language/language.php");

if($_SESSION['type']==2){
  header( "Location:home.php");
  exit;
}

$qry = "SELECT * FROM tbl_settings where id='1'";
$result = mysqli_query($mysqli, $qry);
$settings_row = mysqli_fetch_assoc($result);

if (isset($_POST['submit'])) {

  $img_res = mysqli_query($mysqli, "SELECT * FROM tbl_settings WHERE id='1'");
  $img_row = mysqli_fetch_assoc($img_res);


  if ($_FILES['app_logo']['name'] != "") {

    unlink('images/' . $img_row['app_logo']);

    $app_logo = $_FILES['app_logo']['name'];
    $pic1 = $_FILES['app_logo']['tmp_name'];

    $tpath1 = 'images/' . $app_logo;
    copy($pic1, $tpath1);
    $data = array(
      'app_name'  =>  $_POST['app_name'],
      'app_logo'  =>  $app_logo,
      'app_description'  => addslashes($_POST['app_description']),
      'app_version'  =>  $_POST['app_version'],
      'app_author'  =>  $_POST['app_author'],
      'app_contact'  =>  $_POST['app_contact'],
      'app_email'  =>  $_POST['app_email'],
      'app_website'  =>  $_POST['app_website'],
      'app_developed_by'  =>  $_POST['app_developed_by']
    );
  } else {

    $data = array(
      'app_name'  =>  $_POST['app_name'],
      'app_description'  => addslashes($_POST['app_description']),
      'app_version'  =>  $_POST['app_version'],
      'app_author'  =>  $_POST['app_author'],
      'app_contact'  =>  $_POST['app_contact'],
      'app_email'  =>  $_POST['app_email'],
      'app_website'  =>  $_POST['app_website'],
      'app_developed_by'  =>  $_POST['app_developed_by']
    );
  }

  $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");
  $_SESSION['msg'] = "11";
  header("Location:settings.php");
  exit;
}
if (isset($_POST['verify_purchase_submit'])) {

  $envato_buyer = verify_envato_purchase_code(trim($_POST['envato_purchase_code']));

  if ($_POST['envato_buyer_name'] != '' and $envato_buyer->buyer == $_POST['envato_buyer_name'] and $envato_buyer->item->id == '21158310') {
    $data = array(
      'envato_buyer_name' => $_POST['envato_buyer_name'],
      'envato_purchase_code' => $_POST['envato_purchase_code'],
      'envato_buyer_email' => $_POST['envato_buyer_email'],
      'envato_purchased_status' => 1,
      'package_name' => $_POST['package_name']
    );

    $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");


    $config_file_default    = "includes/app.default";
    $config_file_name       = "api.php";

    $config_file_path       = $config_file_name;

    $config_file = file_get_contents($config_file_default);

    $f = @fopen($config_file_path, "w+");

    if (@fwrite($f, $config_file) > 0) {
      echo "done";
    }

    $protocol = strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, 5)) == 'https' ? 'https' : 'http';

    $admin_url = $protocol . '://' . $_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']) . '/';

    verify_data_on_server($envato_buyer->item->id, $envato_buyer->buyer, $_POST['envato_purchase_code'], 1, $admin_url, $_POST['package_name'], $_POST['envato_buyer_email']);

    // Successfull Verification done !

    $_SESSION['msg'] = "19";
    header("Location:settings.php");
    exit;
  } else {
    $data = array(
      'envato_buyer_name' => $_POST['envato_buyer_name'],
      'envato_purchase_code' => $_POST['envato_purchase_code'],
      'envato_buyer_email' => $_POST['envato_buyer_email'],
      'envato_purchased_status' => 0,
      'package_name' => $_POST['package_name']
    );

    $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");

    // Verfication failed

    $_SESSION['msg'] = "18";
    header("Location:settings.php");
    exit;
  }
}


if (isset($_POST['notification_submit'])) {

  $data = array(
    'onesignal_app_id' => $_POST['onesignal_app_id'],
    'onesignal_rest_key' => $_POST['onesignal_rest_key'],
  );

  $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");

  $_SESSION['msg'] = "11";
  header("Location:settings.php");
  exit;
}
if (isset($_POST['api_submit'])) {

  $data = array(
    'api_latest_limit'  =>  $_POST['api_latest_limit'],
    'api_cat_order_by'  =>  $_POST['api_cat_order_by'],
    'api_cat_post_order_by'  =>  $_POST['api_cat_post_order_by']
  );

  $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");

  $_SESSION['msg'] = "11";
  header("Location:settings.php");
  exit;
}
if (isset($_POST['admob_submit'])) {
  $data = array(
    'publisher_id'  =>  $_POST['publisher_id'],
    'interstital_ad'  =>  $_POST['interstital_ad'],
    'interstital_ad_id'  =>  $_POST['interstital_ad_id'],
    'interstital_ad_click'  =>  $_POST['interstital_ad_click'],
    'banner_ad'  =>  $_POST['banner_ad'],
    'banner_ad_id'  =>  $_POST['banner_ad_id']
  );

  $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");

  $_SESSION['msg'] = "11";
  header("Location:settings.php");
  exit;
}

if (isset($_POST['app_pri_poly'])) {

  $data = array(
    'app_privacy_policy'  =>  addslashes($_POST['app_privacy_policy'])
  );

  $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");

  $_SESSION['msg'] = "11";
  header("Location:settings.php");
  exit;
}

if (isset($_POST['app_terms_con'])) {
  $data = array(
    'app_terms_conditions'  =>  addslashes($_POST['app_terms_conditions'])
  );

  $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");

  $_SESSION['msg'] = "11";
  header("Location:settings.php");
  exit;
}


if (isset($_POST['app_email_config'])) {

  $data = array(
    'app_from_email'  =>  $_POST['app_from_email'],
    'app_admin_email'  =>  $_POST['app_admin_email']
  );

  $settings_edit = Update('tbl_settings', $data, "WHERE id = '1'");

  $_SESSION['msg'] = "11";
  header("Location:settings.php");
  exit;
}



?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title ">
          Herramientas
        </h3>

      </div>
      <div>

      </div>
    </div>
  </div>
  <!-- END: Subheader -->
  <div class="m-content">
    <?php if (isset($_SESSION['msg'])) { ?>
    <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <?php echo $client_lang[$_SESSION['msg']]; ?>
      </div>
    </div>
    <?php unset($_SESSION['msg']);
    } ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
          <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
              <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active" data-toggle="tab" href="#verify_purchase" role="tab">
                    <i class="flaticon-share m--hide"></i>
                    Verificar Compra
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link " data-toggle="tab" href="#app_settings_tab_1" role="tab">
                    <i class="flaticon-share m--hide"></i>
                      Configurar App    
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_email_config" role="tab">
                    Email
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#ad_settings" role="tab">
                    Anuncio
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_settings_tab_2" role="tab">
                    API
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#notification_settings" role="tab">
                    Notificacion
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_settings_tab_3" role="tab">
                    Politica de Privacidad
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_settings_tab_4" role="tab">
                    Terminos y Condiciones
                  </a>
                </li>

              </ul>
            </div>

          </div>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="verify_purchase">
              <form action="" name="verify_purchase" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data" id="">

                <div class="section">
                  <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                      <label class="col-md-3 control-label">Envato Nombre Comprador :-</label>
                      <div class="col-md-9">
                        <input type="text" name="envato_buyer_name" id="envato_buyer_name" value="<?php echo $settings_row['envato_buyer_name']; ?>" class="form-control" placeholder="viaviwebtech" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label class="col-md-3 control-label">Envato Codigo Compra :-

                        <p class="control-label-help">(<a href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-Is-My-Purchase-Code" target="_blank">Donde esta mi codigo de compra?</a>)</p>
                      </label>
                      <div class="col-md-9">
                        <input type="text" name="envato_purchase_code" id="envato_purchase_code" value="<?php echo $settings_row['envato_purchase_code']; ?>" class="form-control" placeholder="xxxx-xxxx-xxxx-xxxx-xxxx" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label class="col-md-3 control-label">Envato Email Comprador :-
                      </label>
                      <div class="col-md-9">
                        <input type="text" name="envato_buyer_email" id="envato_buyer_email" value="<?php echo $settings_row['envato_buyer_email']; ?>" class="form-control" placeholder="info@viaviweb.in" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group m-form__group row">
                      <label class="col-md-3 control-label">Nombre Paquete Android :-
                        <p class="control-label-help">(Mas Informacion: Android Doc)</p>
                      </label>
                      <div class="col-md-9">
                        <input type="text" name="package_name" id="package_name" value="<?php echo $settings_row['package_name']; ?>" class="form-control" placeholder="com.example.myapp" autocomplete="off">
                      </div>
                    </div>

                    <div class="m-portlet__foot m-portlet__foot--fit">
                      <div class="col-md-9 col-md-offset-3">
                        <div class="m-form__actions">
                          <div class="row">
                            <div class="col-4"></div>
                            <div class="col-9">
                              <button type="submit" name="verify_purchase_submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                Guardar Cambios
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div class="tab-pane " id="app_settings_tab_1">
              <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                <div class="m-portlet__body">
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Nombre App
                    </label>
                    <div class="col-9">
                      <input class="form-control m-input" type="text" name="app_name" value="<?php echo $settings_row['app_name']; ?>" autocomplete="off">
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Imagen de Perfil
                    </label>
                    <div class="col-9">
                      <input type="file" id="" name="app_logo" class="form-control m-input">
                      <br />
                      <?php if ($settings_row['app_logo'] != "") { ?>
                      <img type="image" src="images/<?php echo $settings_row['app_logo'].'?id=3'; ?>" alt="image" style="width: 100px;height: 100px"/>
                      <?php } else { ?>
                      <img type="image" src="assets/images/add-image.png" alt="image" />
                      <?php } ?>

                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Descripcion App :-</label>
                    <div class="col-md-9">
                      <textarea name="app_description" id="app_description" class="form-control m-input"><?php echo stripslashes($settings_row['app_description']); ?></textarea>

                      <script>
                        CKEDITOR.replace('app_description');
                      </script>
                    </div>
                  </div>
                  <div class="form-group">&nbsp;</div>
                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Version App :-</label>
                    <div class="col-md-9">
                      <input type="text" name="app_version" id="app_version" value="<?php echo $settings_row['app_version']; ?>" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Autor :-</label>
                    <div class="col-md-9">
                      <input type="text" name="app_author" id="app_author" value="<?php echo $settings_row['app_author']; ?>" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Contacto :-</label>
                    <div class="col-md-9">
                      <input type="text" name="app_contact" id="app_contact" value="<?php echo $settings_row['app_contact']; ?>" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Email :-</label>
                    <div class="col-md-9">
                      <input type="text" name="app_email" id="app_email" value="<?php echo $settings_row['app_email']; ?>" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Website :-</label>
                    <div class="col-md-9">
                      <input type="text" name="app_website" id="app_website" value="<?php echo $settings_row['app_website']; ?>" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Desarrollado por :-</label>
                    <div class="col-md-9">
                      <input type="text" name="app_developed_by" id="app_developed_by" value="<?php echo $settings_row['app_developed_by']; ?>" class="form-control" autocomplete="off">
                    </div>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <button type="submit" name="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Guardar Cambios
                        </button>

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane" id="app_email_config">
              <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                <div class="m-portlet__body">


                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Servidor Email :-<br><small>Dominio email E.g.: info@example.com</small></label>
                    <div class="col-md-9">
                      <input type="email" name="app_from_email" id="app_from_email" value="<?php echo $settings_row['app_from_email']; ?>" class="form-control" required autocomplete="off">
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Notificacion Email del Pedido  :-</label>
                    <div class="col-md-9">
                      <input type="email" name="app_admin_email" id="app_admin_email" value="<?php echo $settings_row['app_admin_email']; ?>" class="form-control" required autocomplete="off">
                    </div>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <button type="submit" name="app_email_config" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Guardar Cambios
                        </button>

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane" id="ad_settings">
              <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                <div class="m-portlet__body">

                  <div class="form-group m-form__group row">
                    <label class="col-md-3 control-label">Editor ID :-</label>
                    <div class="col-md-9">
                      <input type="text" name="publisher_id" id="publisher_id" value="<?php echo $settings_row['publisher_id']; ?>" class="form-control m-input" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Anuncio Banner :-
                    </label>
                    <div class="col-9">
                      <select name="banner_ad" id="banner_ad" class="form-control m-input">
                        <option value="true" <?php if ($settings_row['banner_ad'] == 'true') { ?>selected<?php } ?>>True</option>
                        <option value="false" <?php if ($settings_row['banner_ad'] == 'false') { ?>selected<?php } ?>>False</option>

                      </select>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Anuncio Banner ID :-
                    </label>
                    <div class="col-9">
                      <input type="text" name="banner_ad_id" id="banner_ad_id" value="<?php echo $settings_row['banner_ad_id']; ?>" class="form-control m-input" autocomplete="off">

                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Anuncios Interstital :-
                    </label>
                    <div class="col-9">
                      <select name="interstital_ad" id="interstital_ad" class="form-control m-input">
                        <option value="true" <?php if ($settings_row['interstital_ad'] == 'true') { ?>selected<?php } ?>>True</option>
                        <option value="false" <?php if ($settings_row['interstital_ad'] == 'false') { ?>selected<?php } ?>>False</option>

                      </select>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Anuncio Interstital ID :-
                    </label>
                    <div class="col-9">
                      <input type="text" name="interstital_ad_id" id="interstital_ad_id" value="<?php echo $settings_row['interstital_ad_id']; ?>" class="form-control m-input" autocomplete="off">

                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Clicks a Anuncio Interstital :-
                    </label>
                    <div class="col-9">
                      <input type="text" name="interstital_ad_click" id="interstital_ad_click" value="<?php echo $settings_row['interstital_ad_click']; ?>" class="form-control m-input" autocomplete="off">

                    </div>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <button type="submit" name="admob_submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Guardar Cambios
                        </button>

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div class="tab-pane" id="notification_settings">
              <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                <div class="m-portlet__body">

                  <div class="form-group m-form__group row">
                    <label class="col-md-3 control-label">OneSignal App ID :-</label>
                    <div class="col-md-9">
                      <input type="text" name="onesignal_app_id" id="onesignal_app_id" value="<?php echo $settings_row['onesignal_app_id']; ?>" class="form-control m-input" autocomplete="off">
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      OneSignal Rest Key :-
                    </label>
                    <div class="col-9">
                      <input type="text" name="onesignal_rest_key" id="onesignal_rest_key" value="<?php echo $settings_row['onesignal_rest_key']; ?>" class="form-control m-input" autocomplete="off">

                    </div>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <button type="submit" name="notification_submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Guardar Cambios
                        </button>

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane" id="app_settings_tab_2">
              <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                <div class="m-portlet__body">


                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Ultimo Limite
                    </label>
                    <div class="col-9">
                      <input class="form-control m-input" type="text" name="api_latest_limit" value="<?php echo $settings_row['api_latest_limit']; ?>" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Programar Lista de Pedidod por 
                    </label>
                    <div class="col-9">
                      <select class="form-control m-input" name="api_cat_order_by" id="api_cat_order_by">
                        <option value="pid" <?php if ($settings_row['api_cat_order_by'] == 'pid') { ?>selected<?php } ?>>ID</option>
                        <option value="program_name" <?php if ($settings_row['api_cat_order_by'] == 'program_name') { ?>selected<?php } ?>>Name</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label for="example-text-input" class="col-3 col-form-label">
                      Programar Video de Pedido por 
                    </label>
                    <div class="col-9">
                      <select class="form-control m-input" name="api_cat_post_order_by" id="api_cat_post_order_by">
                        <option value="ASC" <?php if ($settings_row['api_cat_post_order_by'] == 'ASC') { ?>selected<?php } ?>>ASC</option>
                        <option value="DESC" <?php if ($settings_row['api_cat_post_order_by'] == 'DESC') { ?>selected<?php } ?>>DESC</option>
                      </select>
                    </div>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <button type="submit" name="api_submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Guardar Cambios
                        </button>

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div class="tab-pane" id="app_settings_tab_3">
              <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                <div class="m-portlet__body">


                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Politicas de Privacidad App :-</label>
                    <div class="col-md-9">
                      <textarea name="app_privacy_policy" id="app_privacy_policy" class="form-control m-input"><?php echo stripslashes($settings_row['app_privacy_policy']); ?></textarea>

                      <script>
                        CKEDITOR.replace('app_privacy_policy');
                      </script>
                    </div>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <button type="submit" name="app_pri_poly" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Guardar Cambios
                        </button>

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>

            <div class="tab-pane" id="app_settings_tab_4">
              <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                <div class="m-portlet__body">


                  <div class="form-group m-form__group row">
                    <label class="col-3 col-form-label">Terminos y Condiciones App :-</label>
                    <div class="col-md-9">
                      <textarea name="app_terms_conditions" id="app_terms_conditions" class="form-control m-input"><?php echo stripslashes($settings_row['app_terms_conditions']); ?></textarea>

                      <script>
                        CKEDITOR.replace('app_terms_conditions');
                      </script>
                    </div>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <div class="row">
                      <div class="col-3"></div>
                      <div class="col-9">
                        <button type="submit" name="app_terms_con" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                          Guardar Cambios
                        </button>

                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- end:: Body -->


<?php include("includes/footer.php"); ?>

<script type="text/javascript">
  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
  });

  var activeTab = localStorage.getItem('activeTab');
  if (activeTab) {
    $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
  }
</script>