<?php include("includes/header.php");

if( isset($_SERVER['HTTPS'] ) ) {  

    $file_path = 'https://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/api.php';
  }
  else
  {
    $file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/api.php';
  }?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                  Example API urls
                </h3>
                
              </div>
              <div>
                
              </div>
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Example API urls
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">
                 
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                      <pre><code class="html">
                      <br><b>API URL</b>&nbsp; <?php echo $file_path;?>    
					            <br><b>Home</b>(Method: get_home)
                      <br><b>Category List</b>(Method: get_category)
                      <br><b>Restaurants List By Cat ID</b>(Method: get_restaurant_by_cat_id)(Parameter:cat_id)
                      <br><b>Restaurants List By Top Rated</b>(Method: top_rated)
                      <br><b>Restaurants Details</b>(Method: get_restaurant_details)(Parameter:restaurant_id)
					            <br><b>Restaurants Search</b>(Method: get_restaurant_search)(Parameter:search_type,search_text)
                      <br><b>All Restaurants List</b>>(Method: get_all_restaurants)
                      <br><b>Restaurants Menu Cat List</b>(Method: get_restaurant_by_menu_cat_id)(Parameter:menu_cat)
                      <br><b>Menu List by Cat ID</b>(Method: get_menu_list_cat_id)(Parameter:menu_list_cat_id)
                      <br><b>Cart List</b>(Method: get_cart_list)(Parameter:user_id)
                      <br><b>Cart Add/Update</b>(Method: get_cart_add_update)(Parameter:user_id,rest_id,menu_id,menu_name,restaurants_id,menu_qty,menu_price)
                      <br><b>Cart Item Delete</b>(Method: get_cart_item_delete)(Parameter:cart_id)
                      <br><b>Cart Item Empty</b>(Method: get_cart_item_empty)(Parameter:user_id)
                      <br><b>Order Cancel</b>(Method: get_order_cancel)(Parameter:user_id,order_unique_id)
                      <br><b>Order Placed</b>(Method: get_order_add)(Parameter:user_id,order_address,order_comment,cat_ids)
                      <br><b>Order List</b>(Method: user_order_list)(Parameter:user_id)
                      <br><b>User Register</b>(Method: user_register)(Parameter:name,email,password,phone,user_image)                    
					            <br><b>User Login</b>(Method: users_login)(Parameter:email,password)
					            <br><b>User Profile</b>(Method: user_profile)(Parameter:id)
					            <br><b>User Profile Update</b>(Method: user_profile_update)(Parameter:user_id,name,email,password,phone,address,user_image)
					              <br><b>Forgot Password</b>(Method: user_forgot_pass)(Parameter:email)
                      <br><b>Rating</b>(Method: rating)(Parameter:user_id,rate,msg,rest_id)
                      <br><b>App Details</b>(Method: get_app_details)
                     
					           </code></pre>
          

                </div>
          

                <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>       
