<?php 
  include('includes/header.php');
  include('includes/function.php');
  include('language/language.php'); 

  require_once("thumbnail_images.class.php");
   
   if($_SESSION['type']==2){
    header( "Location:home.php");
    exit;
  }
  
  //Get program
  $rest_qry="SELECT * FROM tbl_restaurants ORDER BY id";
  $rest_result=mysqli_query($mysqli,$rest_qry); 
  if(isset($_POST['submit']) and isset($_GET['add']))
  {   
      
      if(!isset($_POST['email']))
      {
          $_SESSION['msg']="9";
          header("location:add_admin_user.php?add=yes");   
          exit;
        
      }
      if(!isset($_POST['admin_restaurant_id'])){
        $_SESSION['msg']="20";
         header("location:add_admin_user.php?add=yes");   
         exit;
      }
      $qry = "SELECT * FROM tbl_admin WHERE email = '".$_POST['email']."'"; 
      $result = mysqli_query($mysqli,$qry);
      $row = mysqli_fetch_assoc($result);
      if($row['email']!="")
      {
         $_SESSION['msg']="16";
         header("location:add_admin_user.php?add=yes");   
         exit;
        
      }      
      else
      {
          $data = array(
          'username'=> $_POST['email'], 
          'full_name'  =>  $_POST['full_name'],
          'email'  =>  $_POST['email'],
          'password'  =>  password_hash($_POST['password'], PASSWORD_DEFAULT, ['cost' => 10]),
          'phone'  =>  $_POST['phone'],
          'status' => 1,
          'image'  =>  ""
          );

          $user_qry = Insert('tbl_admin',$data);
          $qry = "SELECT * FROM tbl_admin WHERE email = '".$_POST['email']."'"; 
          $result = mysqli_query($mysqli,$qry);
          $row = mysqli_fetch_assoc($result);
          $dataRestaurants = array(
            'id_admin' => $row['id'],
            'id_restaurants' => $_POST['admin_restaurant_id']
          );
          insert('tbl_admin_restaurants',$dataRestaurants);
        
          $_SESSION['msg']="10";
          header("location:manage_admin_users.php");   
          exit;
      }
  }
  
  if(isset($_GET['admin_user_id']) and isset($_GET['admin_restaurant_id']))
  {
       
      $user_qry="SELECT * FROM tbl_admin where id='".$_GET['admin_user_id']."'";
      $user_result=mysqli_query($mysqli,$user_qry);
      $user_row=mysqli_fetch_assoc($user_result);
      $restaurant_qry="SELECT * FROM tbl_admin_restaurants where id_admin='".$_GET['admin_user_id']."' 
      and id_restaurants ='".$_GET['admin_restaurant_id']."'";
      $restaurant_result=mysqli_query($mysqli,$restaurant_qry);
      $restaurant_row=mysqli_fetch_assoc($restaurant_result);
      $restaurante_id = $restaurant_row['id_restaurants'];
  }
  
  if(isset($_POST['submit']) and isset($_POST['admin_user_id']) and isset($_POST['admin_restaurant_id']))
  {
      
    if($_POST['password']!="")
    {
      $data = array(
      'username'=> $_POST['email'], 
      'full_name'  =>  $_POST['full_name'],
      'email'  =>  $_POST['email'],
      'password'  =>  password_hash($_POST['password'], PASSWORD_DEFAULT, ['cost' => 10]),
      'phone'  =>  $_POST['phone'],
      'status' => $_POST['status'],
      'image'  =>  ""
      );
    }
    else
    {
      $data = array(
      'username'=> $_POST['email'], 
      'full_name'  =>  $_POST['full_name'],
      'email'  =>  $_POST['email'],      
      'phone'  =>  $_POST['phone'],
      'status' => $_POST['status'],
      'image'  =>  ""
      );
    }
 
    
      
    if($_POST['admin_restaurant_id'] != $_POST['before_admin_restaurant_id']){
        $where = "id_admin = '".$_POST['admin_user_id']."' and id_restaurants ='".$_POST['before_admin_restaurant_id']."'";
        Delete('tbl_admin_restaurants', $where);
        $dataRestaurants = array(
          'id_admin' => $_POST['admin_user_id'],
          'id_restaurants' => $_POST['admin_restaurant_id']
        );
        insert('tbl_admin_restaurants',$dataRestaurants);
      }
      $user_edit=Update('tbl_admin', $data, "WHERE id = '".$_POST['admin_user_id']."'");
      if ($user_edit > 0){
        
        $_SESSION['msg']="11";
        header("Location:add_admin_user.php?admin_user_id=".$_POST['admin_user_id']."&admin_restaurant_id=".$_POST['admin_restaurant_id']);
        exit;
      }   
    
   
  }
  
  
?>
  

 <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      <?php if(isset($_GET['admin_user_id'])){?>Editar<?php }else{?>Agregar<?php }?> Usuario (Admin)
                    </h3>
                  </div>
                </div>
              </div>
              <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 
              <!--begin::Form-->
              <form action="" name="addeditcategory" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
              
                 <input  type="hidden" name="admin_user_id" value="<?php echo $_GET['admin_user_id'];?>" />
                 <input  type="hidden" name="before_admin_restaurant_id" value="<?php echo $restaurante_id;?>" />
                 <input  type="hidden" name="status" value="<?php echo $user_row['status'];?>" />

                <div class="m-portlet__body">
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Nombre
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="full_name" id="name" value="<?php if(isset($_GET['admin_user_id'])){echo $user_row['full_name'];}?>" placeholder="Nombre Completo" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Email
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="email" id="email" value="<?php if(isset($_GET['admin_user_id'])){echo $user_row['email'];}?>" placeholder="Email" autocomplete="off" required>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Password
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="password" class="form-control m-input" name="password" id="password" value="" placeholder="" <?php if(!isset($_GET['admin_user_id'])){?>required<?php }?>>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Telefono
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="phone" id="phone" value="<?php if(isset($_GET['admin_user_id'])){echo $user_row['phone'];}?>" placeholder="Telefono" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Restaurante
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <select name="admin_restaurant_id" id="m_select2" class="form-control m_select2" required>
                        <option value="">--Seleccione Restaurante--</option>
                        <?php
                            while($rest_row=mysqli_fetch_array($rest_result))
                            {
                        ?>                       
                        <option value="<?php echo $rest_row['id'];?>" <?php if(isset($_GET['admin_user_id']) && $rest_row['id']==$restaurant_row['id_restaurants']) {?>selected<?php }?>><?php echo $rest_row['restaurant_name'];?></option>                           
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>   
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" name="submit" class="btn btn-brand">
                          Guardar
                        </button>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>