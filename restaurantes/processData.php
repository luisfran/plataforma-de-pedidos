<?php 
	require("includes/connection.php");
	require("includes/function.php");
	require("language/language.php");

	$response=array();

	switch ($_POST['action']) {
		case 'toggle_status':
			$id=$_POST['id'];
			$for_action=$_POST['for_action'];
			$column=$_POST['column'];
			$tbl_id=$_POST['tbl_id'];
			$table_nm=$_POST['table'];

			if($for_action=='active'){
				$data = array($column  =>  '1');
			    $edit_status=Update($table_nm, $data, "WHERE $tbl_id = '$id'");
			}else{
				$data = array($column  =>  '0');
			    $edit_status=Update($table_nm, $data, "WHERE $tbl_id = '$id'");
			}
			
	      	$response['status']=1;
	      	$response['action']=$for_action;
	      	echo json_encode($response);
			break;

		case 'order_status':
			$id=$_POST['id'];
			$status=$_POST['status'];

			$data = array('status'  =>  $status);
			$edit_status=Update('tbl_order_details', $data, "WHERE order_unique_id = '$id'");

			$_SESSION['msg']="11";
	      	$response['status']=1;
	      	echo json_encode($response);
			break;

		case 'removeData':
			$id=$_POST['id'];
			$tbl_nm=$_POST['tbl_nm'];
			$tbl_id=$_POST['tbl_id'];

			Delete($tbl_nm,$tbl_id.'='.$id);

	      	$response['status']=1;
	      	echo json_encode($response);
			break;

		case 'multi_delete':

			$ids=implode(",", $_POST['id']);

			if($ids==''){
				$ids=$_POST['id'];
			}

			$tbl_nm=$_POST['tbl_nm'];

			if($tbl_nm=='tbl_restaurants'){

				$sql="SELECT * FROM $tbl_nm WHERE `id` IN ($ids)";
				$res=mysqli_query($mysqli, $sql);
				while ($row=mysqli_fetch_assoc($res)){
					if($row['restaurant_image']!="")
					{
						unlink('images/'.$row['restaurant_image']);
						unlink('images/thumbs/'.$row['restaurant_image']);
					}

				}
				$deleteSql="DELETE FROM $tbl_nm WHERE `id` IN ($ids)";

				mysqli_query($mysqli, $deleteSql);
			}

			else if($tbl_nm=='tbl_category'){

				$sqlCategory="SELECT * FROM $tbl_nm WHERE `cid` IN ($ids)";
				$res=mysqli_query($mysqli, $sqlCategory);
				while ($row=mysqli_fetch_assoc($res)){
					if($row['category_image']!="")
					{
						unlink('images/'.$row['category_image']);
						unlink('images/thumbs/'.$row['category_image']);
					}

				}
				$deleteSql="DELETE FROM $tbl_nm WHERE `cid` IN ($ids)";

				mysqli_query($mysqli, $deleteSql);
			}

			else if($tbl_nm=='tbl_users'){

				$deleteCart="DELETE FROM tbl_cart WHERE `user_id` IN ($ids)";
				mysqli_query($mysqli, $deleteCart);

				$sql="DELETE FROM tbl_order_items WHERE `user_id` IN ($ids)";
				mysqli_query($mysqli, $sql);

				$sql="DELETE FROM tbl_order_details WHERE `user_id` IN ($ids)";
				mysqli_query($mysqli, $sql);

				$sql="DELETE FROM tbl_users WHERE `id` IN ($ids)";
				mysqli_query($mysqli, $sql);
			}

			else if($tbl_nm=='tbl_order'){

				echo $sql="DELETE FROM tbl_order_items WHERE `order_id` IN ('".$ids."')";
				mysqli_query($mysqli, $sql);

				$sql="DELETE FROM tbl_order_details WHERE `order_unique_id` IN ('".$ids."')";
				mysqli_query($mysqli, $sql);
			}
			
			$_SESSION['msg']="12";
	      	$response['status']=1;
	      	echo json_encode($response);
			break;
		
		default:
			# code...
			break;
	}
