<?php
    include("includes/connection.php");
    include("language/language.php");

  if(isset($_SESSION['admin_name']))
  {
    header("Location:home.php");
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en" >
  <!-- begin::Head -->
  <head>
    <meta charset="utf-8" />
    <title><?php echo APP_NAME;?></title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
    <style type="text/css">
      #applogo{
        width: 47%;
      }
      body{
        background: #008c15;
      }
      .m-login.m-login--2.m-login-2--skin-1 .m-login__container .m-login__form .form-control{
        background: #FFF !important;
        color: #000 !important;
      }
    </style>
  </head>
  <!-- end::Head -->
  <!-- end::Body -->
  <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
      <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-1" id="m_login">
        <div class="m-grid__item m-grid__item--fluid  m-login__wrapper">
          <div class="m-login__container">
            <div class="m-login__logo">
              <a href="#">
                <img src="images/expresslogo.png?id=2';?>" id="applogo" />
              </a>
            </div>
            <div class="m-login__signin">
            
              <form class="m-login__form m-form" action="login_db.php" method="post">
                <?php if(isset($_SESSION['msg'])){?>
                <div class="m-alert m-alert--outline alert alert-danger alert-dismissible" role="alert">      <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>     <span><?php echo $client_lang[$_SESSION['msg']]; ?></span>    </div>
              <?php unset($_SESSION['msg']);}?>
              
                <div class="form-group m-form__group">
                  <input class="form-control m-input"   type="text" placeholder="correo electrónico" name="username" autocomplete="off">
                </div>
                <div class="form-group m-form__group">
                  <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Contraseña" name="password">
                </div>
                <!--<div class="row m-login__form-sub">
                   
                  <div class="col m--align-right m-login__form-right">
                    <a href="javascript:;" id="m_login_forget_password" class="m-link">
                      Forget Password ?
                    </a>
                  </div>
                </div>-->
                <div class="m-login__form-action">
                  <button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
                    Entrar
                  </button>
                </div>
              </form>
            </div>
            
            <div class="m-login__forget-password">
              <div class="m-login__head">
                <h3 class="m-login__title">
                  Olvido Password ?
                </h3>
                <div class="m-login__desc">
                  Ingresa tu email para resetear tu password:
                </div>
              </div>
              <form class="m-login__form m-form" action="">
                <div class="form-group m-form__group">
                  <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                </div>
                <div class="m-login__form-action">
                  <button id="m_login_forget_password_submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                    Solicitar
                  </button>
                  &nbsp;&nbsp;
                  <button id="m_login_forget_password_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                    Cancel
                  </button>
                </div>
              </form>
            </div>
             
          </div>
        </div>
      </div>
    </div>
    <!-- end:: Page -->
    <!--begin::Base Scripts -->
    <script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Base Scripts -->
     
  </body>
  <!-- end::Body -->
</html>
