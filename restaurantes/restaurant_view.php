<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
	 
	
	if(isset($_GET['restaurant_id']))
	{
			 
		$rest_qry="SELECT * FROM tbl_restaurants where id='".$_GET['restaurant_id']."'";
    $rest_result=mysqli_query($mysqli,$rest_qry);
    $rest_row=mysqli_fetch_assoc($rest_result);

	}
  else
  {
    header( "Location:manage_restaurants.php");
    exit; 
  }
 
 
  $qry_cat="SELECT COUNT(*) as num FROM tbl_menu_category WHERE restaurant_id='".$_GET['restaurant_id']."'";
  $total_category= mysqli_fetch_array(mysqli_query($mysqli,$qry_cat));
  $total_category = $total_category['num'];

  $qry_menu="SELECT COUNT(*) as num FROM tbl_menu_list WHERE rest_id='".$_GET['restaurant_id']."'";
  $total_menu= mysqli_fetch_array(mysqli_query($mysqli,$qry_menu));
  $total_menu = $total_menu['num'];

  $qry_order="SELECT COUNT(*) as num FROM tbl_order_items WHERE rest_id='".$_GET['restaurant_id']."'";
  $total_order= mysqli_fetch_array(mysqli_query($mysqli,$qry_order));
  $total_order = $total_order['num'];
?>
 
	 <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <?php 
            $curr_page='Dashboard';
            include_once 'includes/header_2.php';

          ?>
              <div class="col-lg-9">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                
                  <div class="tab-content">
                     
                     <!--begin:: Widgets/Stats-->
              <div class="m-portlet">
                <div class="m-portlet__body  m-portlet__body--no-padding">
                  <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-6">
                      <!--begin::Total Profit-->
                      <a href="manage_menu_category.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Total Categorias

                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            Menu por Categoria
                          </span>
                          <span class="m-widget24__stats m--font-brand">
                            <?php echo $total_category;?>
                          </span>
                          <div class="m--space-40"></div>
                            
                        </div>
                      </div>
                      </a>
                      <!--end::Total Profit-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                      <!--begin::New Feedbacks-->
                      <a href="manage_menu_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Total Menus
                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            Lista de Menu
                          </span>
                          <span class="m-widget24__stats m--font-info">
                            <?php echo $total_menu;?>
                          </span>
                          <div class="m--space-40"></div>
                           
                        </div>
                      </div>
                      </a>
                      <!--end::New Feedbacks-->
                    </div>
                    </div>
                    <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-6">
                      <!--begin::New Orders-->
                      <a href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Pedidos
                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            Lista de Pedidos
                          </span>
                          <span class="m-widget24__stats m--font-danger">
                            <?php echo $total_order;?>
                          </span>
                          <div class="m--space-40"></div>
                           
                        </div>
                      </div>
                    </a>
                      <!--end::New Orders-->
                    </div>
                    
                  </div>
                </div>
              </div>            
              <!--end:: Widgets/Stats-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>       
