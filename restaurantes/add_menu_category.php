<?php include("includes/header.php");

require("includes/function.php");
require("language/language.php");
require_once("thumbnail_images.class.php");
  
  $rest_qry="SELECT * FROM tbl_restaurants where id='".$_GET['restaurant_id']."'";
  $rest_result=mysqli_query($mysqli,$rest_qry);
  $rest_row=mysqli_fetch_assoc($rest_result);
   
  if(isset($_POST['submit']) and isset($_GET['add']))
  {
   
       $data = array( 
         'restaurant_id'  =>  $_POST['restaurant_id'],
         'category_name'  =>  $_POST['category_name']
           );    

    $qry = Insert('tbl_menu_category',$data);  
 

    $_SESSION['msg']="10";
 
    header( "Location:manage_menu_category.php?restaurant_id=".$_POST['restaurant_id']);
    exit; 

     
    
  }
  
  if(isset($_GET['cat_id']))
  {
       
      $qry="SELECT * FROM tbl_menu_category where cid='".$_GET['cat_id']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

  }
  if(isset($_POST['submit']) and isset($_POST['cat_id']))
  {
      
        $data = array(
                 'restaurant_id'  =>  $_POST['restaurant_id'],
                 'category_name'  =>  $_POST['category_name']
              );  
 
    $category_edit=Update('tbl_menu_category', $data, "WHERE cid = '".$_POST['cat_id']."'");
 
    $_SESSION['msg']="11"; 
    header( "Location:add_menu_category.php?cat_id=".$_POST['cat_id']."&restaurant_id=".$_POST['restaurant_id']);
    exit;
 
  }
 

?>       


        <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <?php 
            $curr_page='Menu Category';
            include_once 'includes/header_2.php';

          ?>
              <div class="col-lg-9">
                 <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      <?php if(isset($_GET['cat_id'])){?>Editar<?php }else{?>Agregar<?php }?> Categoria del Menu 
                    </h3>
                  </div>
                </div>
              </div>
              <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?>
              <!--begin::Form-->
              <form action="" name="addeditcategory" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
              
                 <input  type="hidden" name="cat_id" value="<?php echo $_GET['cat_id'];?>" />
                 <input  type="hidden" name="restaurant_id" value="<?php echo $_GET['restaurant_id'];?>" />

                <div class="m-portlet__body">
             
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Nombre Categoria
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="category_name" id="category_name" value="<?php if(isset($_GET['cat_id'])){echo $row['category_name'];}?>" placeholder="Introduzca el Nombre de la Categoria" autocomplete="off">
                    </div>
                  </div>
                   
                   
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" name="submit" class="btn btn-brand">
                          Guardar
                        </button>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>       
