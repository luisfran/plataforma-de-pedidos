<?php 
  include('includes/header.php'); 
  include('includes/function.php');
	include('language/language.php');  


  if(isset($_POST['user_search']))
  {

      $keyword=addslashes(trim($_POST['search_value']));
      $sql="";
      if($_SESSION['type']==1){
        $sql="SELECT * FROM tbl_users WHERE tbl_users.`name` LIKE '%".$keyword."%' or tbl_users.`email` like '%".addslashes($_POST['search_value'])."%' ORDER BY tbl_users.id DESC";
      } else {
        $sql="SELECT user.* FROM tbl_users user inner join tbl_users_restaurants userest on user.id=userest.id_user
        WHERE user.`name` LIKE '%".$keyword."%' or user.`email` 
        like '%".addslashes($_POST['search_value'])."%' and userest.id_restaurants ='".$_SESSION['id_restaurant']."'  
        ORDER BY user.id DESC";
      }

      $users_result=mysqli_query($mysqli,$sql);
  }
  else
  {

    $tableName="tbl_users";		
    $targetpage = "manage_users.php"; 	
    $limit = 15; 

    $query = "SELECT COUNT(*) as num FROM $tableName";
    $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
    $total_pages = $total_pages['num'];

    $stages = 3;
    $page=0;
    if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
    }
    if($page){
      $start = ($page - 1) * $limit; 
    }else{
      $start = 0;	
    }	

    if($_SESSION['type']==1){
      $users_qry="SELECT * FROM tbl_users ORDER BY tbl_users.`id` DESC LIMIT $start, $limit";
    } else {
      $users_qry="SELECT user.* FROM tbl_users user inner join tbl_users_restaurants userest on user.id=userest.id_user 
      WHERE userest.id_restaurants ='".$_SESSION['id_restaurant']."'
      ORDER BY user.`id` DESC LIMIT $start, $limit";
    }
      

    $users_result=mysqli_query($mysqli,$users_qry);

  }
	
?>


 <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Administracion de Usuarios
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">
                
                <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 
                <!--begin: Search Form -->

                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                      <div class="form-group m-form__group row align-items-center">
                        <form  method="post" action="" class="m-form">

                            <div class="col-md-12">
                              <div class="m-input-icon m-input-icon--left">
                                <div class="input-group">
                                  <input type="text" name="search_value" class="form-control form-control-warning" placeholder="Buscar por..." required autocomplete="off">
                                  <span class="input-group-btn">
                                    <button class="btn btn-brand" type="submit" name="user_search">
                                      Go!
                                    </button>
                                  </span>
                                </div>
                              </div>
                            </div>
                        </form>    
                      </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <a href="add_user.php?add=yes" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                          <i class="la la-plus"></i>
                          <span>
                            Agregar Usuario
                          </span>
                        </span>
                      </a>
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="table-responsive" id="local_data">
                    <table class="table table-sm">
              <thead class="thead-default">
                <tr>                  
                  <th>Nombre</th>						 
        				  <th>Email</th>
        				  <th>Telefono</th>
         				  <th>Status</th>	 
                  <th class="cat_action_list">Acciones</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    $i=0;
                    while($users_row=mysqli_fetch_array($users_result))
                    {         
                ?>
                <tr scope="row">                 
                  <td><?php echo $users_row['name'];?></td>
		              <td><?php echo $users_row['email'];?></td>   
		              <td><?php echo $users_row['phone'];?></td>             
		              <td>
                    <?php if($users_row['status']!="0"){?>
                      <a href="" class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$users_row['id']?>" data-action="deactive" data-column="status" title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Enable</span></span></a>

                      <?php }else{?>
                      <a href="" class="toggle_btn_a" href="javascript:void(0)" data-id="<?=$users_row['id']?>" data-action="active" data-column="status" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Disable </span></span></a>
                    <?php }?>
                  </td>
                  <td>
                    <a href="add_user.php?user_id=<?php echo $users_row['id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar detalles">
                      <i class="la la-edit"></i>
                    </a>
                    <?php if($_SESSION['type']==1){?>
                      <a href="" data-id="<?php echo $users_row['id'];?>" class="btn_delete_a m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar">
                        <i class="la la-trash"></i>
                      </a>
                    <?php }?>

                  </td>
                     
                </tr>
                <?php
                $i++;
                }
                ?> 
              </tbody>
            </table>

                </div>
          <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php if(!isset($_POST["user_search"])){ include("pagination.php");}?>
                  </nav>
                </div>
          </div>


                <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>    

<script type="text/javascript">

  $(".toggle_btn_a").on("click",function(e){
    e.preventDefault();
    
    var _for=$(this).data("action");
    var _id=$(this).data("id");
    var _column=$(this).data("column");
    var _table='tbl_users';

    $.ajax({
      type:'post',
      url:'processData.php',
      dataType:'json',
      data:{id:_id,for_action:_for,column:_column,table:_table,'action':'toggle_status','tbl_id':'id'},
      success:function(res){
          console.log(res);
          if(res.status=='1'){
            location.reload();
          }
        }
    });

  });

  $(".btn_delete_a").click(function(e){
      e.preventDefault();

      var _ids = $(this).data("id");

      if(_ids!='')
      {
        if(confirm("Estas seguro de Eliminar esto?")){
          $.ajax({
            type:'post',
            url:'processData.php',
            dataType:'json',
            data:{id:_ids,'action':'multi_delete','tbl_nm':'tbl_users'},
            success:function(res){
                console.log(res);
                if(res.status=='1'){
                  location.reload();
                }
                else if(res.status=='-2'){
                  alert(res.message);
                }
              }
          });
        }
      }
  });

</script> 