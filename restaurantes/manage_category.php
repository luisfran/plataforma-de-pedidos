<?php include("includes/header.php");

require("includes/function.php");
require("language/language.php");

  if(isset($_POST['cat_search']))
  {
    $keyword=addslashes(trim($_POST['search_value']));
    $sql = "SELECT * FROM tbl_category 
      WHERE tbl_category.`category_name` LIKE '%$keyword%' ORDER BY tbl_category.`category_name` DESC";
    $result = mysqli_query($mysqli, $sql);
  }
  else
  {
    $tableName = "tbl_category";
    $targetpage = "manage_category.php";
    $limit = 10;

    $query = "SELECT COUNT(*) as num FROM $tableName";
    $total_pages = mysqli_fetch_array(mysqli_query($mysqli, $query));
    $total_pages = $total_pages['num'];

    $stages = 1;
    $page = 0;
    if (isset($_GET['page'])) {
      $page = mysqli_real_escape_string($mysqli, $_GET['page']);
    }
    if ($page) {
      $start = ($page - 1) * $limit;
    } else {
      $start = 0;
    }

    $quotes_qry = "SELECT * FROM tbl_category 
        ORDER BY tbl_category.cid DESC LIMIT $start, $limit";
    $result = mysqli_query($mysqli, $quotes_qry);
  }


  function get_total_item($cat_id)
  { 
    global $mysqli;   

    $sql="SELECT COUNT(*) as num FROM tbl_restaurants WHERE cat_id='$cat_id'";
     
    $total = mysqli_fetch_array(mysqli_query($mysqli,$sql));
    $total = $total['num'];
     
    return $total;

  }

?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <div class="m-content">

    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Administrar Categorias

            </h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">

        </div>
      </div>
      <div class="m-portlet__body">

        <?php if (isset($_SESSION['msg'])) { ?>
        <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <?php echo $client_lang[$_SESSION['msg']]; ?>
          </div>
        </div>
        <?php unset($_SESSION['msg']);
        } ?>

        <!--begin: Search Form -->

        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
          <div class="row align-items-center">
            <div class="col-xl-8 order-2 order-xl-1">
              <div class="form-group m-form__group row align-items-center">
                <form method="post" action="" class="m-form">

                  <div class="col-md-12">
                    <div class="m-input-icon m-input-icon--left">

                      <div class="input-group">
                        
                        <input type="text" name="search_value" class="form-control form-control-warning" placeholder="Buscar por..." value="<?php if(isset($_POST['search_value'])){ echo $_POST['search_value']; } ?>" required autocomplete="off">
                        <span class="input-group-btn">
                          <button class="btn btn-brand" type="submit" name="cat_search">
                            Go!
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
              <?php if($_SESSION['type']==1){?>
                <a href="add_category.php?add=yes" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                  <span>
                    <i class="la la-plus"></i>
                    <span>
                      Agregar Categoria
                    </span>
                  </span>
                </a>
              <?php }?>
              <div class="m-separator m-separator--dashed d-xl-none"></div>
            </div>
          </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="table-responsive" id="local_data">
          <table class="table table-sm">
            <thead class="thead-default">
              <tr>
                <th>Categoria</th>
                <th>Imagen de la Categoria</th>
                <th class="cat_action_list">Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $i = 0;
                while ($row = mysqli_fetch_array($result)) {
              ?>
              <tr scope="row">
                <td><?php echo $row['category_name']; ?>(<?php echo get_total_item($row['cid']);?>)</td>
                <td>
                  <img src="images/<?php echo $row['category_image']; ?>" width="150" height="100" style="border-radius: 10px"/>
                </td>
                <td>
                  <?php if($_SESSION['type']==1){?>
                    <a href="add_category.php?cat_id=<?php echo $row['cid']; ?>"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar detalles"> <i class="la la-edit"></i> </a>

                    <a href="" data-id="<?php echo $row['cid'];?>" class="btn_delete_a m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar"> <i class="la la-trash"></i> </a>
                  <?php } else {?>
                    <a href="#"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar detalles"> <i class="la la-edit"></i> </a>

                    <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar"> <i class="la la-trash"></i> </a>
                  <?php }?>
              </tr>
              <?php

                $i++;
              }
              ?>
            </tbody>
          </table>

        </div>
        <div class="col-md-12 col-xs-12">
          <div class="pagination_item_block">
            <nav>
              <?php if (!isset($_POST["cat_search"])) {
                include("pagination.php");
              } ?>
            </nav>
          </div>
        </div>


        <!--end: Datatable -->
      </div>
    </div>
  </div>
</div>
</div>
<!-- end:: Body -->

<?php include("includes/footer.php"); ?>

<script type="text/javascript">

  $(".btn_delete_a").click(function(e){
      e.preventDefault();

      var _ids = $(this).data("id");

      if(_ids!='')
      {
        if(confirm("Estas Seguro de Eliminar este Registro?")){
          $.ajax({
            type:'post',
            url:'processData.php',
            dataType:'json',
            data:{id:_ids,'action':'multi_delete','tbl_nm':'tbl_category'},
            success:function(res){
                console.log(res);
                if(res.status=='1'){
                  location.reload();
                }
                else if(res.status=='-2'){
                  alert(res.message);
                }
              }
          });
        }
      }
  });

</script>