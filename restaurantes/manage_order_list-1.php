<?php 
  include("includes/header.php");
  require("includes/function.php");
  require("language/language.php");

  $statusOrder="";
  if(isset($_POST['order_search']))
  {
    $fechaInicial = strtotime($_POST['fechaInicio']);
    $fechaFinalizacion = strtotime($_POST['fechaFin']);
    if($fechaInicial > $fechaFinalizacion){
      $_SESSION['msg']="22";
      header("Location:manage_order_list.php");
      exit;
    }
    $inicio = $_POST['fechaInicio'];
    $fin = $_POST['fechaFin'];
    $query_search = "SELECT * FROM tbl_order_details
            WHERE date(order_date) between date('".$inicio."') and date('".$fin."')";
    if($_POST['statusOrder'] != ""){
      $query_search = $query_search." and status ='".$_POST['statusOrder']."'";
      $statusOrder = $_POST['statusOrder'];
    }
    if($_SESSION['type']==2){
      $query_search = $query_search." and rest_id ='".$_SESSION['id_restaurant']."'";
    }
    $query_search = $query_search." ORDER BY tbl_order_details.id ";
    $result = mysqli_query($mysqli, $query_search);
  } else {
    $tableName = "tbl_order_details";
    $targetpage = "manage_order_list.php";
    $limit = 10;
    $inicio = date("Y")."-".date("m")."-".date("d");
    $fin = date("Y")."-".date("m")."-".date("d");
    $query = "SELECT COUNT(*) as num FROM $tableName  
          ORDER BY tbl_order_details.id DESC";
    $total_pages = mysqli_fetch_array(mysqli_query($mysqli, $query));
    $total_pages = $total_pages['num'];

    $stages = 1;
    $page = 0;
    if (isset($_GET['page'])) {
      $page = mysqli_real_escape_string($mysqli, $_GET['page']);
    }
    if ($page) {
      $start = ($page - 1) * $limit;
    } else {
      $start = 0;
    }

    if($_SESSION['type']==1){
      $data_qry = "SELECT * FROM tbl_order_details
            WHERE date(order_date) between date('".$inicio."') and date('".$fin."')
            ORDER BY tbl_order_details.id DESC LIMIT $start, $limit";
    } else {
      $data_qry = "SELECT * FROM tbl_order_details
            WHERE rest_id ='".$_SESSION['id_restaurant']."' and date(order_date) between date('".$inicio."') and date('".$fin."')
            ORDER BY tbl_order_details.id DESC LIMIT $start, $limit";
    }
    $result = mysqli_query($mysqli, $data_qry);
    $statusOrder="";

    if (isset($_GET['order_id'])) {

      Delete('tbl_order_items', 'order_id="' . $_GET['order_id'] . '"');
      Delete('tbl_order_details', 'order_unique_id="' . $_GET['order_id'] . '"');

      $_SESSION['msg'] = "12";
      header("Location:manage_order_list.php");
      exit;
    }

    //order status
    if (isset($_GET['status_pending_id'])) {
      $data = array('status'  =>  $_GET['status_value']);

      $edit_status = Update('tbl_order_details', $data, "WHERE order_unique_id = '" . $_GET['status_pending_id'] . "'");

      //$_SESSION['msg']="14";
      header("Location:manage_order_list.php");
      exit;
    }
  }

  function get_user_info($user_id)
  {
    global $mysqli;

    $query1 = "SELECT * FROM tbl_users
      WHERE tbl_users.id='" . $user_id . "'";

    $sql1 = mysqli_query($mysqli, $query1) or die(mysqli_error());
    $data1 = mysqli_fetch_assoc($sql1);

    return $data1;
  }

  function get_order_status($order_id)
  {
    global $mysqli;

    $query1 = "SELECT * FROM tbl_order_details
        WHERE tbl_order_details.order_unique_id='" . $order_id . "'";

    $sql1 = mysqli_query($mysqli, $query1) or die(mysqli_error());
    $data1 = mysqli_fetch_assoc($sql1);

    return $data1['status'];
  }

?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">

  <div class="m-content">

    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Administracion de Pedidos

            </h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">

        </div>
      </div>
      <div class="m-portlet__body">

        <?php if (isset($_SESSION['msg'])) { ?>
        <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <?php echo $client_lang[$_SESSION['msg']]; ?>
          </div>
        </div>
        <?php unset($_SESSION['msg']);
        } ?>
        <form action="" name="search_from" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
          <div class="form-group m-form__group row">
            <label class="col-form-label col-md-2" style="text-align: right">Fecha Inicio:</label>
            <div class="col-md-3">
              <div class="m-input-icon m-input-icon--left">
                <div class="input-group date" id="datepicker" data-provide="datepicker" data-date="<?php echo $hoy; ?>" >
                    <input type="text" class="form-control" value="<?php echo $inicio; ?>" placeholder="Fecha Inicio" name="fechaInicio" readonly >
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
              </div>
            </div>
            <label class="col-form-label col-md-3" style="text-align: right">Fecha Fin:</label>
            <div class="col-md-3">
              <div class="m-input-icon m-input-icon--right">
                <div class="input-group date" id="datepicker1" data-provide="datepicker" data-date="<?php echo $hoy; ?>">
                    <input type="text" class="form-control" value="<?php echo $fin; ?>" placeholder="Fecha Fin" name="fechaFin" readonly >
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
              </div>
            </div>          
          </div>
          <div class="form-group m-form__group row">
            <label class="col-form-label col-md-2" style="text-align: right">Estados Pedidos:</label>
            <div class="col-md-5">
              <select name="statusOrder" id="m_select3" class="form-control m_select2">
                <option value="">--Seleccione Estado--</option>
                <option value="Pending" <?php if($statusOrder!="" && $statusOrder=="Pending") {?>selected<?php }?>>Pendiente</option>
                <option value="Process" <?php if($statusOrder!="" && $statusOrder=="Process") {?>selected<?php }?>>En Proceso</option>
                <option value="Complete" <?php if($statusOrder!="" && $statusOrder=="Complete") {?>selected<?php }?>>Completado</option>
                <option value="Cancel" <?php if($statusOrder!="" && $statusOrder=="Cancel") {?>selected<?php }?>>Cancelado</option>
              </select>
            </div>
            <div class="col-md-1">
              <button type="submit" name="order_search" class="btn btn-brand">
                Buscar
              </button>
              
            </div>
          </div>
        </form>
        <br/>
        <!--begin: Datatable -->
        <div class="m_datatable" id="local_data">
          <table class="table">
            <thead class="thead-default">
              <tr>
                <th>Pedido ID</th>
                <th>Nombre Usuario</th>
                <th>Telefono del Usuario</th>
                <th>Status</th>
                <th class="cat_action_list">Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i = 0;
              while ($row = mysqli_fetch_array($result)) {
                ?>
              <tr scope="row">
                <td><a href="manage_order_list_view.php?order_id=<?php echo $row['order_unique_id']; ?>" title="View Order"><?php echo $row['order_unique_id']; ?></a></td>
                <td><?php echo get_user_info($row['user_id'])['name']; ?></td>
                <td><?php echo get_user_info($row['user_id'])['phone']; ?></td>
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn <?php if (get_order_status($row['order_unique_id']) == "Complete") { ?>btn-success<?php } else if (get_order_status($row['order_unique_id']) == "Process") { ?> btn-warning <?php } else { ?>btn-danger<?php } ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo get_order_status($row['order_unique_id']); ?></button>
                    <div class="dropdown-menu" x-placement="top-start">
                      <a class="dropdown-item" href="" data-id="<?php echo $row['order_unique_id']; ?>" data-status="Pending">Pendiente</a>
                      <a class="dropdown-item" href="" data-id="<?php echo $row['order_unique_id']; ?>" data-status="Process">En Proceso</a>
                      <a class="dropdown-item" href="" data-id="<?php echo $row['order_unique_id']; ?>" data-status="Complete">Completo</a>
                      <a class="dropdown-item" href="" data-id="<?php echo $row['order_unique_id']; ?>" data-status="Cancel">Cancelar</a>
                    </div>
                  </div>
                </td>
                <td>

                  <a href="manage_order_list_view.php?order_id=<?php echo $row['order_unique_id']; ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View Order"> <i class="la la-eye"></i> </a>
                  <?php if($_SESSION['type']==1){?>
                    <a href="" data-id="<?php echo $row['order_unique_id'];?>" class="btn_delete_a m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="la la-trash"></i> </a>
                  <?php }?>
              </tr>
              <?php

                $i++;
              }
              ?>

            </tbody>
          </table>
        </div>
        <div class="col-md-12 col-xs-12">
          <div class="pagination_item_block">
            <nav>
              <?php include("pagination.php"); ?>
            </nav>
          </div>
        </div>


        <!--end: Datatable -->
      </div>
    </div>
  </div>
</div>
</div>
<!-- end:: Body -->
<?php include("includes/footer.php"); ?>

<script type="text/javascript">

  $(".dropdown-item").click(function(e){
    e.preventDefault();

    var _id=$(this).data("id");
    var _status=$(this).data("status");

    if(confirm("Esta Seguro de Cambiar el Status?")){
      $.ajax({
        type:'post',
        url:'processData.php',
        dataType:'json',
        data:{'id':_id,'status':_status,'action':'order_status'},
        success:function(res){
            console.log(res);
            if(res.status=='1'){
              location.reload();
            }
            else if(res.status=='-2'){
              alert(res.message);
            }
          }
      });
    }

  });

  $(".btn_delete_a").click(function(e){
      e.preventDefault();

      var _ids = $(this).data("id");

      if(_ids!='')
      {
        if(confirm("Esta Eeguro de Eliminar esto?")){
          $.ajax({
            type:'post',
            url:'processData.php',
            dataType:'json',
            data:{id:_ids,'action':'multi_delete','tbl_nm':'tbl_order'},
            success:function(res){
                console.log(res);
                if(res.status=='1'){
                  location.reload();
                }
                else if(res.status=='-2'){
                  alert(res.message);
                }
              }
          });
        }
      }
  });

  $.fn.datepicker.dates['en'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabdo"],
    daysShort: ["Dom", "Lun", "Mar", "Mier", "Jue", "Vier", "Sab"],
    daysMin: ["Dom", "Lun", "Mar", "Mier", "Jue", "Vier", "Sab"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octuber", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Mes Actual",
    clear: "Limpiar",
    format: "yyyy-mm-dd",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0    
};

$('#datepicker').datepicker({
  autoclose: true,
  todayBtn:true,
  todayHighlight:true
});
$('#datepicker1').datepicker({
  autoclose: true,
  todayBtn:true,
  todayHighlight:true
});
</script>