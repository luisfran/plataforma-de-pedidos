<!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">
            Restaurante : <?php echo $rest_row['restaurant_name'];?>
          </h3>
        </div>
        <div>
           
        </div>
      </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
      <div class="row">
        <div class="col-lg-3">
          <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__body">
              <div class="m-card-profile">
                <div class="m-card-profile__title m--hide">
                  Tu Dashboard
                </div>
                <div class="m-card-profile__pic">
                  <div class="m-card-profile__pic-wrapper">
                    <img src="images/<?php echo $rest_row['restaurant_image'];?>" style="width: 100px;height: 100px;border-radius: 10px;" alt=""/>
                  </div>
                </div>
                <div class="m-card-profile__details">
                  <span class="m-card-profile__name">
                    <?php echo $rest_row['restaurant_name'];?>
                  </span>
                   
                    <?php echo $rest_row['restaurant_address'];?>
                   
                </div>
              </div>
              <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                <li class="m-nav__separator m-nav__separator--fit"></li>
                <li class="m-nav__section m--hide">
                  <span class="m-nav__section-text">
                    Seccion
                  </span>
                </li>
                 <li class="m-nav__item <?php if($curr_page=='Dashboard'){ echo 'm-nav__item--active';} ?>">
                  <a href="restaurant_view.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                    <i class="m-nav__link-icon fa fa-dashboard "></i>
                    <span class="m-nav__link-text">
                      Dashboard
                    </span>
                  </a>
                </li>
                <li class="m-nav__item <?php if($curr_page=='Menu Category'){ echo 'm-nav__item--active';} ?>">
                  <a href="manage_menu_category.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                    <i class="m-nav__link-icon flaticon-share"></i>
                    <span class="m-nav__link-text">
                      Menu Categoria
                    </span>
                  </a>
                </li>
                <li class="m-nav__item <?php if($curr_page=='Menu List'){ echo 'm-nav__item--active';} ?>">
                  <a href="manage_menu_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                    <i class="m-nav__link-icon flaticon-chat-1"></i>
                    <span class="m-nav__link-text">
                      Lista Menu
                    </span>
                  </a>
                </li>
                <li class="m-nav__item <?php if($curr_page=='Order List'){ echo 'm-nav__item--active';} ?>">
                  <a href="manage_rest_order_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                    <i class="m-nav__link-icon fa fa-cart-arrow-down"></i>
                    <span class="m-nav__link-text">
                      Lista Pedidos
                    </span>
                  </a>
                </li> 
              </ul>
              <div class="m-portlet__body-separator"></div>
              
            </div>
          </div>
        </div>