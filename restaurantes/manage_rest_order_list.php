<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
    

    if(isset($_GET['restaurant_id']))
    {
         
      $rest_qry="SELECT * FROM tbl_restaurants where id='".$_GET['restaurant_id']."'";
      $rest_result=mysqli_query($mysqli,$rest_qry);
      $rest_row=mysqli_fetch_assoc($rest_result);

    }
    else
    {
      header("Location:manage_restaurants.php");
      exit; 
    }
      

      $tableName="tbl_order_items";   
      $targetpage = "manage_rest_order_list.php"; 
      $limit = 10; 
      
      $query = "SELECT COUNT(*) as num FROM $tableName  
      WHERE tbl_order_items.rest_id='".$_GET['restaurant_id']."'
      ORDER BY tbl_order_items.id DESC";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];
      
      $stages = 1;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
        $start = ($page - 1) * $limit; 
      }else{
        $start = 0; 
        }


     $data_qry="SELECT * FROM tbl_order_items
       WHERE tbl_order_items.rest_id='".$_GET['restaurant_id']."' 
       GROUP BY(order_id) ORDER BY tbl_order_items.id DESC LIMIT $start, $limit"; 
     $result=mysqli_query($mysqli,$data_qry);
 
	
	if(isset($_GET['order_id']))
	{

		Delete('tbl_order_items','order_id="'.$_GET['order_id'].'"');
    Delete('tbl_order_details','order_unique_id="'.$_GET['order_id'].'"');
 
		$_SESSION['msg']="12";
		header( "Location:manage_rest_order_list.php?restaurant_id=".$_GET['restaurant_id']);
		exit;
		
	}	

   //order status
if(isset($_GET['status_pending_id']))
{
   $data = array('status'  =>  $_GET['status_value']);
  
   $edit_status=Update('tbl_order_details', $data, "WHERE order_unique_id = '".$_GET['status_pending_id']."'");
  
   //$_SESSION['msg']="14";
   header( "Location:manage_rest_order_list.php?restaurant_id=".$_GET['restaurant_id']);
   exit;
   exit;
}

  function get_user_info($user_id)
  {
    global $mysqli;

    $query1="SELECT * FROM tbl_users
    WHERE tbl_users.id='".$user_id."'";

    $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
    $data1 = mysqli_fetch_assoc($sql1);

    return $data1;
  }

  function get_order_status($order_id)
  {
    global $mysqli;

    $query1="SELECT * FROM tbl_order_details
    WHERE tbl_order_details.order_unique_id='".$order_id."'";

    $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
    $data1 = mysqli_fetch_assoc($sql1);

    return $data1['status'];
  }

  function get_order_address_comment($order_id)
  {
    global $mysqli;

    $query1="SELECT * FROM tbl_order_details
    WHERE tbl_order_details.order_unique_id='".$order_id."'";

    $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
    $data1 = mysqli_fetch_assoc($sql1);

    return $data1;
  }
	 
?>
      <style type="text/css">
        .card{
          margin-top: 5px;
        }
      </style>      
     <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <?php 
            $curr_page='Order List';
            include_once 'includes/header_2.php';

          ?>
              <div class="col-lg-9">
                <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Lista Pedidos
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">

                <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 

                 
                <!--begin: Datatable -->
            <div class="m_datatable" id="local_data">
              <?php 
                $i=0;
                while($row=mysqli_fetch_array($result))
                {         
              ?>
              <div class="card" style="width: 100%;">
                <div class="card-body">
                  <h5 class="card-title">Pedido</h5>
                  <div class="form-group m-form__group row">
                    <p class="card-text col-md-6">Número: <a href="manage_rest_order_list_view.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&user_id=<?php echo $row['user_id']?>&order_id=<?php echo $row['order_id'];?>" title="Ver Orden"><?php echo $row['order_id'];?></a></p>
                    <p class="card-text col-md-6">Nombre: <?php echo get_user_info($row['user_id'])['name']; ?></p>
                    <p class="card-text col-md-6">Dirección envío: <?php echo get_order_address_comment($row['order_id'])['order_address']; ?></p>
                    <p class="card-text col-md-6">Mensaje: <?php echo get_order_address_comment($row['order_id'])['order_comment']; ?></p>
                    <p class="card-text col-md-6">Telefono: <?php echo get_user_info($row['user_id'])['phone']; ?></p>
                    <p class="card-text col-md-2">Estado: </p>
                    <div class="btn-group col-md-3">
                      <button type="button" class="btn <?php if(get_order_status($row['order_id'])=="Complete"){?>btn-success<?php }else if(get_order_status($row['order_id'])=="Process"){?> btn-warning <?php }else{?>btn-danger<?php }?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo get_order_status($row['order_id']);?></button>
                      <div class="dropdown-menu" x-placement="top-start">
                          <a class="dropdown-item" href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&status_pending_id=<?php echo $row['order_id'];?>&status_value=Pending">Pendiente</a>
                          <a class="dropdown-item" href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&status_pending_id=<?php echo $row['order_id'];?>&status_value=Process">En Proceso</a>
                          <a class="dropdown-item" href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&status_pending_id=<?php echo $row['order_id'];?>&status_value=Complete">Completo</a>                            
                      </div>
                    </div>
                    <p class="card-text col-md-2">Acciones</p>
                    <div class="btn-group col-md-4">
                    <a href="manage_rest_order_list_view.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&user_id=<?php echo $row['user_id']?>&order_id=<?php echo $row['order_id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Vista Pedido"><i class="la la-eye"></i></a>
                      <?php if($_SESSION['type']==1){?>
                        <a href="?order_id=<?php echo $row['order_id'];?>&restaurant_id=<?php echo $_GET['restaurant_id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar" onclick="return confirm('Esta Segudo de Eliminar este Pedido?');"><i class="la la-trash"></i></a>
                      <?php }?>
                    </div>
                  </div>
                </div>
              </div>
              <?php
          
                  $i++;
                }
              ?>    
            </div>
            <div class="col-md-12 col-xs-12">
            <div class="pagination_item_block">
              <nav>
                <?php include("pagination_rest.php");?>
              </nav>
            </div>
          </div>
                <!--end: Datatable -->
              </div>
            </div>
          </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>       
